# OpenLane

## Quantr fork, below are commands to make it works

1. export OPENLANE_IMAGE_NAME=quantrpeter/openlane:current
2. make openlane

if success, you see this

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-1.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-2.png)

!!! Containers/Apps, Volumnes and Dev Environments in Docker Desktop has nothing

3. make pdk

if success, you see this

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-3.png)

4. make mount

if success, you see this

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-4.png)

5. flow.tcl -design spm

this step is to test if it works, if success, you see this

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-5.png)

6. make -f Makefile.quantr

if success, you see this

![](https://www.quantr.foundation/wp-content/uploads/2022/01/compile-openlane-6.png)

